import { Component, OnInit } from '@angular/core';
import "rxjs/Rx";
import { Http, Request, Response, RequestOptionsArgs } from "@angular/http";
import {DosareStatService} from "./dosare-stat.service";
import {Router} from "@angular/router";
import {ConfirmDialog, ConfirmationService} from "primeng/primeng"

@Component({
  selector: 'app-dosare-stat',
  templateUrl: './dosare-stat.component.html',
  styleUrls: ['./dosare-stat.component.css']
})

export class DosareStatComponent implements OnInit{
  users = [];
  display: boolean =false;
  private currentPage: number = 1;
  public per_page: number[] = [
    10,
    20,
    30
  ];
  total_records: number = 0;


  constructor(private http : Http , private dosareService : DosareStatService , private router : Router, private confirmationService: ConfirmationService ){

  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {

      }
    });
  }


  showDialog(){
    this.display = true;
  }
  handleR(event){
    var x =4;
  }
  loadData (event){
    if (event.first === 0) {
      this.currentPage = 1;
    } else {
      this.currentPage = Math.ceil(event.first / event.rows) + 1;
    }
    this.dosareService.getUsers().subscribe(users =>{
      this.users = users;
      this.total_records = users.length;
      console.log(users);
    });
  }
  ngOnInit(): void {
  }


}
