import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUpload, Message} from "primeng/primeng";
import {forEach} from "@angular/router/src/utils/collection";
import {Http, RequestOptions , Headers} from "@angular/http";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-edit-dosare-pensii-alimentare',
  templateUrl: './edit-dosare-pensii-alimentare.component.html',
  styleUrls: ['./edit-dosare-pensii-alimentare.component.css']
})
export class EditDosarePensiiAlimentareComponent implements OnInit {

  constructor(private route: ActivatedRoute , private router : Router,private  http:Http) { }
  msgs: Message[];
  public childrens1 : any[] =[];
  public dosarName: string = null;
  uploadedFiles: any[] = [];
  public childrens : any[]=[];
  public display : boolean=false;
  public pustoi_array : string;
  public index : number = 1;
  public file_storage : any;
  files: any;
  apiEndPoint = "http://192.168.1.69:8080/user/case/upload2";
  public url_for_upload = 'http://192.168.1.69:8080/user/case/fileupload/1';

  public url: string = 'http://192.168.1.69:8080/user/case/fileupload';
  @ViewChild("cerere_file") cerere_file: FileUpload;
  @Input() multiple: boolean = false;
  @ViewChild('fileInput') inputEl: ElementRef;
 // file1 : any;
  onUpload(event) {
    for(let file of event.files) {
      this.uploadedFiles.push(file);
    }
    console.log(this.uploadedFiles  , "123");
    console.log(event.files , "event.files");
    // this.cerere_file.files
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
  }

  ngOnInit() {
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.dosarName = params['dosarName'];
    });
    for (let i = 1; i <10; i++) {
      var text = {'id':i,'name':'','surname' : '' ,'idnp': '' , 'patronimic' : '' , 'date':''};
       this.childrens1.push(text);
    }

    this.pustoi_array = this.childrens1[0].name;
  }
  // onChange(event) {
  //   var files = event.srcElement.files;
  //   console.log(files);
  //
  // }
  // upload() {
  //   let inputEl: HTMLInputElement = this.inputEl.nativeElement;
  //   let fileCount: number = inputEl.files.length;
  //   let formData = new FormData();
  //   if (fileCount > 0) { // a file was selected
  //     for (let i = 0; i < fileCount; i++) {
  //       formData.append('file[]', inputEl.files.item(i));
  //     }
  //     console.log(formData , "data")
  //     this.http.post('https://httpbin.org/post', formData)
  //     // do whatever you do...
  //     // subscribe to observable to listen for response
  //   }
  //
  // }

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);
      let formData1 = {
        fileName : 'test'
      };
      formData.append('info', new Blob([JSON.stringify(formData1)]));

      console.log( new Blob([JSON.stringify(formData1)]) , 'blob');

      let headers = new Headers();
      headers.append('Accept', 'application/json');
      let options = new RequestOptions({ headers: headers });
      this.http.post(`${this.apiEndPoint}`, formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => console.log('success'),
          error => console.log(error)
        )
    }
  }


  onChange(event: any) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('degree_attachment', file, file.name);
      let formData1 = {
        fileName : 'test'
      }
      formData.append("name",  "test_");
      formData.append('info', new Blob([JSON.stringify(formData1)],
        {
          type: "application/json"
        }));
      let headers = new Headers();
      headers.append('Accept', 'multipart/form-data');
      let options = new RequestOptions({ headers: headers });
      this.http.post('https://httpbin.org/post', formData,options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => console.log(formData , "formdata"),
          error => console.log(error)
        )
    }}


  onBasicUpload() {


    // var headers = new Headers();
    // headers.append('Content-Type', 'multipart/form-data' );
    // let options = new RequestOptions({ headers: headers });
    //   var fileFirst = sessionStorage.getItem('file');
    // let postParams = {
    //   doc2 : fileFirst
    // };
    // this.http.post("https://httpbin.org/post",postParams ,options).subscribe(function(res){
    //
    //   console.log(postParams,'3333333333333');
    //
    // });
    // $('#file1')
      // console.log(this.cerere_file.files , "this.cerere_file.files");
    // let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    // let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    // let files: FileList = target.files;
    // this.file = files[0]
   // this.file1 = $('#file1');

  //  console.log(this.file1.file , "event");
    // console.log($('#file2') , "event");

  }

 // public file: File;

  changeListener($event): void {


    console.log($event.target.files[0] , "event");

    // this.files.push($event.target.files[0]);
    // for(let m=0;m<2;m++){
    //
    //   console.log($event.target.files[m] , "event");
    //
    // }

  }


  // testFile(){
  //
  //   console.log(this.files,'fffffffffffffffffffffff')
  //
  //   var formData = new FormData();
  //   formData.append("name", "Name");
  //   formData.append("file",  this.files[0]);
  //
  //
  //   var headers = new Headers();
  //   headers.append('Content-Type', 'multipart/form-data' );
  //   let options = new RequestOptions({ headers: headers });
  //
  //
  //   this.http.post("http://192.168.1.69:8080/user/case/fileupload",formData , options).subscribe(function(res){
  //
  //     console.log(res,'3333333333333');
  //
  //   });
  //
  // }

  // addNewElement(id : number){
  //   console.log(id , "IDIDIDIDIDIDID");
  //   this.index = id-1;
  //   // this.display = true;
  // }
  onComplete(){
    this.router.navigateByUrl("dash/dosareStat")
  }

  postFile(): void {


    // var formData = new FormData();
    // formData.append("name", "Name");
    // formData.append("file",  this.files[0]);

    var headers = new Headers();
     headers.append('Content-Type', 'application/json;multipart/form-data;boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');
     headers.append("Accept", 'application/json');

    let options = new RequestOptions({ headers: headers });

    var formData = new FormData();
    formData.append("file",  this.cerere_file.files[0]);

    let formData1 = {
      name: 'test',
      file: this.cerere_file.files[0]
    }
    // formData.append("name",  "test_");
    formData.append('info', JSON.stringify(formData1));

    console.log(formData , "formdata");


    this.http.post('http://192.168.1.69:8080/user/case/fileupload4' , formData1, {
      headers: headers
    }).subscribe(function (res) {
      console.log(res , "res");
    })

  }


}
