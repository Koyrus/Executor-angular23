import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {
  public profileData: any[] = [];

  constructor(private http :Http) { }

  // ngOnInit() {
  //   this.http.get("http://192.168.1.69:8080/executor/admin/profiles")
  //     .subscribe(function (res) {
  //     console.log(res);
  //     const thisref = this;
  //     thisref.profileData = res.json();
  //     console.log(thisref.profileData  , "thisref.profileData")
  //
  //   })
  // }
  ngOnInit() {
    var thisref= this;
    this.http.get("http://192.168.1.69:8080/executor/admin/profiles" , {
    }).subscribe(function (res) {
      thisref.profileData = res.json();
      console.log(thisref.profileData)
    })
  }
}
