import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUpload, Message} from "primeng/primeng";
import {forEach} from "@angular/router/src/utils/collection";
import {Http, RequestOptions , Headers} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {FormControl} from "@angular/forms";
import * as moment from "moment";
import _date = moment.unitOfTime._date;
const MY_DATE_FORMATS = {
  parse: {
    dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
  }
};

@Component({
  selector: 'app-edit-dosare-pensii-alimentare',
  templateUrl: './edit-dosare-pensii-alimentare.component.html',
  styleUrls: ['./edit-dosare-pensii-alimentare.component.css']
})


export class EditDosarePensiiAlimentareComponent implements OnInit {



  constructor(private route: ActivatedRoute , private router : Router,private  http:Http) { }
  msgs: Message[];
  public disable_file : boolean = true;
  public childrens1 : any[] =[];
  public dosarName: string = null;
  uploadedFiles: any[] = [];
  public childrens : any[]=[];
  public display : boolean=false;
  public pustoi_array : string;
  public index : number = 1;
  public file_storage : any;
  public file_name : any;
  public emitent_result : any[] = [];
  // files: any;
  public id_file_type : number = 1;
  public url_for_upload = 'http://192.168.1.69:8080/user/case/fileupload/' + this.id_file_type;
  public choose_file_type : number = null;
  public url: string = 'http://192.168.1.69:8080/user/case/fileupload';
  @ViewChild("cerere_file") cerere_file: FileUpload;
  @Input() multiple: boolean = false;
  @ViewChild('fileInput') inputEl: ElementRef;
  stareDosar : any;
  emitent_instance : any;
  doc_executor : any;
  inst_emit_id : any;
  date_from : Date;
  // date = new FormControl(new Date());
  date;
  // serializedDate = new FormControl((new Date()).toISOString())
 // file1 : any;F

  onChangeFunction(){
      if (this.choose_file_type != null){
        console.log("success");
        this.disable_file = false;
      }
  }

  ngOnInit() {
    this.date = new Date()

    var thisref = this;
    this.http.get("http://192.168.1.69:8080/executor/admin/nomenclator/instance/emitent" , {
    }).subscribe(function (res) {
      console.log(res);
      thisref.emitent_result = res.json();
      console.log(thisref.emitent_result , "priority");

    });


    this.route.params.subscribe(function(params){
      thisref.dosarName = params['dosarName'];
    });
    for (let i = 1; i <10; i++) {
      var text = {'id':i,'name':'','surname' : '' ,'idnp': '' , 'patronimic' : '' , 'date':''};
       this.childrens1.push(text);
    }

    this.pustoi_array = this.childrens1[0].name;
  }

  onBasicUpload() {}

  onComplete(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      stareDosar : this.stareDosar,
      emitent_instance : this.emitent_instance,
      doc_executor : this.doc_executor,
      inst_emit_id : this.inst_emit_id,
      date_from : this.date.toLocaleDateString('ru-RU')

    };
    this.http.post("url" ,postParams , options).subscribe(function (res) {
      console.log(res);
    })

    // this.router.navigateByUrl("dash/dosareStat");
  }



  postFile(): void {

    var headers = new Headers();
    headers.append('Content-Type', 'application/json;multipart/form-data;boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');

    let options = new RequestOptions({ headers: headers });

    var formData = new FormData();
    formData.append("file",  this.cerere_file.files[0]);

    this.http.post(this.url_for_upload , {
      headers: headers
    }).subscribe(function (res) {
      console.log(res , "res");
    })

  }


}
