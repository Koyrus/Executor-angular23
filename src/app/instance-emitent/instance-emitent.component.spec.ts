import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanceEmitentComponent } from './instance-emitent.component';

describe('InstanceEmitentComponent', () => {
  let component: InstanceEmitentComponent;
  let fixture: ComponentFixture<InstanceEmitentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanceEmitentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanceEmitentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
