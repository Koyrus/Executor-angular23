import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {ConfirmationService, Message} from "primeng/primeng";


@Component({
  selector: 'app-nomenclatoare',
  templateUrl: './nomenclatoare.component.html',
  styleUrls: ['./nomenclatoare.component.css']
})
export class NomenclatoareComponent implements OnInit {
  // banksresults: any;
  public banksresult : any;
  public bank_name : any;
  public results : any[] = [];
  public created_nom : any[]=[];
  public bank_edit : any[]=[];
  msgs: Message[] = [];
  msgsError:Message[]=[];
  displayDialog: boolean;
  displayBank : boolean;
  display: boolean = false;
  public new_nomenclator : boolean = false;

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }
  ngOnInit() {
    var thisref = this;
    this.http.get("http://192.168.1.69:8080/executor/admin/nomenclator/banks" , {
    }).subscribe(function (res) {
      console.log(res);
      thisref.banksresult = res.json();
      console.log(thisref.banksresult , "results");

    })
  }

  dateCredentials = {
    bank: this.bank_name,
  };

  deletebank(bank) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: bank.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.banksresult.indexOf(bank);
        console.log(index, "index");
        this.banksresult = this.banksresult.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }
  showDialog(bank) {
    if(bank != null){
      this.bank_edit = bank;
      this.new_nomenclator = false;
    }else {
      this.bank_edit = [];
      this.new_nomenclator = true;
    }
    this.display = true;
  }
  editBank(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.bank_edit['id'],
      name : this.bank_edit['name'],
      type_nomenclator: "BANK",

    };

    console.log(postParams , "postParams");


    this.http.post( "http://192.168.1.69:8080/executor/admin/nomenclator/save", postParams , options).subscribe(
        response => {
          this.created_nom = response.json();
          if(this.new_nomenclator){
            this.banksresult.push(this.created_nom);
            this.msgs = [];
            this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
          }

        }, error => {
          console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
        }
      );


    // console.log( this.created_nom , "212")

    this.display = false;
    console.log(this.banksresult);




  }


  activatedNomBank(bank){
    console.log(bank, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: bank.id,
      is_active: bank.is_active,
      type_nomenclator: "BANK",
    };
    console.log(!bank.is_active, "!organization.is_active");
    this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }


}
