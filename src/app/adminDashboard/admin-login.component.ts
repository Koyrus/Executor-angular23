import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  constructor(private http:Http , private router : Router) {

  }

  usersTable(){
    this.router.navigateByUrl('/adminDashboard/users');
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : -1
    };
    this.http.post("http://192.168.1.69:8080/executor/admin/users" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        console.log(postParams , "Post params")
      });
  }



// /adminDashboard/users
  ngOnInit() {
  }

}
