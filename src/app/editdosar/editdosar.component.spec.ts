import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditdosarComponent } from './editdosar.component';

describe('EditdosarComponent', () => {
  let component: EditdosarComponent;
  let fixture: ComponentFixture<EditdosarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditdosarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditdosarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
