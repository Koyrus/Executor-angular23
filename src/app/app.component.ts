import {Component, NgModule, OnInit} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {DosareStatService} from "./dosare-stat/dosare-stat.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers : [DosareStatService]
})
@NgModule({
  imports : [BrowserModule]
})
export class AppComponent implements OnInit{

  ngOnInit() {

  }


}

