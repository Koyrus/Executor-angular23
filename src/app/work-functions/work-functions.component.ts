import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Message} from "primeng/primeng";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-work-functions',
  templateUrl: './work-functions.component.html',
  styleUrls: ['./work-functions.component.css']
})
export class WorkFunctionsComponent implements OnInit {
  public workFunctions: any;
  public function_edit: any[] = [];
  display: boolean = false;
  public new_function: boolean = false;
  msgs: Message[] = [];
  public created_function_nom: any[] = [];
  displayDialog: boolean;


  constructor(private http: Http , private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    var thisref = this;
    this.http.get("http://192.168.1.69:8080/executor/admin/nomenclator/workfunctions", {}).subscribe(function (res) {
      console.log(res);
      thisref.workFunctions = res.json();

    })
  }

  showDialog(functionn) {
    if (functionn != null) {
      this.function_edit = functionn;
      this.new_function = false;
    } else {
      this.function_edit = [];
      this.new_function = true;
    }
    this.display = true;
  }


  saveFunction(functionn) {
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.function_edit['id'],
      name: this.function_edit['name'],
      type_nomenclator: "WORK_FUNCTIONS",
    };

    console.log(postParams, "postParams");


    this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/save", postParams, options).subscribe(
      response => {
        this.created_function_nom = response.json();
        if (this.new_function) {
          this.workFunctions.push(this.created_function_nom);

        }
        this.msgs = [];
        this.msgs.push({severity: 'success', summary: 'Info Message', detail: 'Successfuly'});

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity: 'error', summary: 'Info Message', detail: 'Error'});
      }
    );

    this.display = false;
    console.log(this.workFunctions);
  }

  activatedNomFunction(functionn){
    console.log(functionn, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: functionn.id,
      is_active: functionn.is_active,
      type_nomenclator: "WORK_FUNCTION",
    };
    console.log(!functionn.is_active, "!organization.is_active");
    this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }

  deleteFunction(func) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: func.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");




        this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.workFunctions.indexOf(func);
        console.log(index, "index");
        this.workFunctions = this.workFunctions.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }





}

