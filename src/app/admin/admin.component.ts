import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private http : Http , private router: Router) { }

  ngOnInit() {
  }
  stateLoginAdmin() {
    this.router.navigateByUrl("adminDashboard");
}
}
