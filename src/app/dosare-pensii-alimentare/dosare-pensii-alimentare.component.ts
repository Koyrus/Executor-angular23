import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dosare-pensii-alimentare',
  templateUrl: './dosare-pensii-alimentare.component.html',
  styleUrls: ['./dosare-pensii-alimentare.component.css']
})
export class DosarePensiiAlimentareComponent implements OnInit {
  public users : any[]=[];
  public userrs : any[]=[];
  public id_dosar : string = 'new';
  display: boolean =false;
  private currentPage: number = 1;
  public per_page: number[] = [
    10,
    20,
    30
  ];
  total_records: number = 0;

  constructor(private http:Http, private router:Router) { }

  ngOnInit() {
    var thisref = this;
      this.http.get("https://randomuser.me/api/?inc=gender,name,picture,location&results=20&nat=gb" ,{}).subscribe(function (res) {
        thisref.users = res.json();
        thisref.userrs = thisref.users['results'];
        console.log(thisref.userrs, "Users")
      });


  }

  createOrganization(){
    this.router.navigateByUrl('/dash/editPensiiAlimentare/:'+this.id_dosar)
  }

}
