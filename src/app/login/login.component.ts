import {Component, NgModule, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@NgModule({
  imports : [BrowserModule]
})
export class LoginComponent implements OnInit {

  username : any;
  password : any;
  constructor(private router: Router){

  }
  ngOnInit() {

  }

  Login(){
    if(this.username === 'test@test.com' && this.password === '1'){
      console.log("Done");
      this.router.navigateByUrl('dash')
    } else console.log("Error");


  }
}
