import { Component, OnInit } from '@angular/core';
import {ConfirmationService, Message} from "primeng/primeng";
import {Router} from "@angular/router";
import {Http, RequestOptions , Headers} from "@angular/http";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  public payment_result : any[]=[];
  public payment_name : any;
  public payment_edit : any[]=[];
  display: boolean = false;
  public new_payment : boolean = false;
  msgs: Message[] = [];
  public payment_nom : any[]=[];
  // public new_payment : boolean = false;
  displayDialog: boolean;

  constructor(private http : Http ,private router : Router , private confirmationService: ConfirmationService) {

  }

  ngOnInit() {
    var thisref = this;
    this.http.get("http://192.168.1.69:8080/executor/admin/nomenclator/paymentsmode" , {
    }).subscribe(function (res) {
      console.log(res);
      thisref.payment_result = res.json();
      console.log(thisref.payment_result , "payment_result");

    })
  }
  showDialog(payment) {
    if(payment != null){
      this.payment_edit = payment;
      this.new_payment = false;
    }else {
      this.payment_edit = [];
      this.new_payment = true;
    }
    this.display = true;
  }

  savePayment(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: this.payment_edit['id'],
      name : this.payment_edit['name'],
      type_nomenclator: "PAYMENT_MODE",

    };

    console.log(postParams , "postParams");


    this.http.post( "http://192.168.1.69:8080/executor/admin/nomenclator/save", postParams , options).subscribe(
      response => {
        this.payment_nom = response.json();
        if(this.new_payment){
          this.payment_result.push(this.payment_nom);
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      }
    );
    this.display = false;
    console.log(this.payment_result);
  }



  public activatedNomPayment(payment){
    console.log(payment, "user");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: -1,
      id_nomenclator: payment.id,
      is_active: payment.is_active,
      type_nomenclator: "PAYMENT_MODE",
    };
    console.log(!payment.is_active, "!organization.is_active");
    this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/active", postParams , options).subscribe(function (res) {
      console.log(res);
    });
  }


  deletePayment(payment) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_nomenclator: payment.id,
          is_delete: true
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
        console.log(postParams , "postParams");
        this.http.post("http://192.168.1.69:8080/executor/admin/nomenclator/delete", postParams , options).subscribe(function (res) {
          console.log(res);
        });
        let index = this.payment_result.indexOf(payment);
        console.log(index, "index");
        this.payment_result = this.payment_result.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }


}
